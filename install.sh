#!/bin/bash

install_prezto() {
    # clone the presto repo

    git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"

    # get prezto's default configuration
    shopt -s extglob
    for dotfile in $HOME/.dotfiles/home/*; do
        ln -s "$dotfile" "$HOME/.$(basename $dotfile)"
    done

    # change default shell to zsh
    chsh -s /bin/zsh
}

install_prezto
