Dotfiles
===

#How to
...

#TODO

cscope?

## General setup

### Vim
Install (vim-plug)[https://github.com/junegunn/vim-plug]:
Unix

```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

Neovim

```
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

git, vim, zsh, ssh
.

## dev env
gcc, make vs build-essential
pip
OSX specific
## consider
ansible
tmux
iTerm2, Konsole, Terminator

# Dotfiles

home/                       # general configs
    .ssh/
        config
    .vim/
        plug.vim
        mappings.vim
        cache/
    .gitconfig
    .vimrc
    .zshrc
playbooks                   # ansible playbooks
    ...
vagrant
    centos7
        Vagrantfile
        bootstrap.sh
    jessie
    trusty
    wheezy
.gitignore

ubuntu-trusty.sh            # apt install...
debian-wheezy.sh            # apt install... gui or not
debian-jessie.sh            # apt install...
osx.sh                      # brew, defaults, etc..


## Tips

### CLI

Emacs mode:
	CTRL+a jump to the beginning of line
	CTRL+e jump to to the end of line
	CTRL+w will delete a word backwards
	CTRL+z pause a command
	CTRL+l clear the screen
	CTRL+d exit current shell
	CTRL+u cuts everything to the left
	CTRL+y pastes what's in the buffer

Vi mode:
	set -o vi

### SSH
keygen
ssh-keygen -t rsa -C 'your@email.com'
ssh config: https://github.com/kdeldycke/dotfiles/blob/master/dotfiles-common/.ssh/config
